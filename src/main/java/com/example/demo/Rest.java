package com.example.demo;

import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "test")
public class Rest {
	@Value(value = "${my.message}")
	public String message;

	@GetMapping
	public String get() {
		//throw new RuntimeException("blaaa");
		return message;
//		Path path = Paths.get("/home/marcel/Downloads/confluent-oss-5.0.0-2.11.tar.gz");
//		Resource resource;
//		try {
//			resource = new UrlResource(path.toUri());
//			if(resource.exists()) {
//	            return ResponseEntity.ok()
//	                    .contentType(MediaType.parseMediaType("application/octet-stream"))
//	                    .contentLength(path.toFile().length())
//	                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"downloadconfluent\"")
//	                    .body(resource);
//	        }
//		} catch (MalformedURLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//        return null;
	}
}
